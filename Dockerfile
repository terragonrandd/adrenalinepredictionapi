FROM terragontech/python
RUN mkdir /api
WORKDIR /api
RUN cd /api
ADD . /api
RUN chmod +x run.sh
RUN pip install --user  -r requirements.txt && pip install -r requirements.txt
EXPOSE 7087
RUN export FLASK_APP=autoapp.py
RUN export FLASK_DEBUG=0
RUN flask run --host=0.0.0.0 --port=5000