from flask import jsonify, make_response, json, request


def check_auth_header():
    username, password = request.authorization.get('username', None), request.authorization.get('password', None)
    if not username or not password:
        return False, "Unable to authenticate user. Set auth header as TEST if you are in test mode."
    if username != "terragonclickprediction" or password != "terragonclickprediction":
        return False, "Invalid authentication header"
    return True, "Successfully authenticated"


class Response:

    def __init__(self):
        self._response_ok = []
        self._response_error = []

    @staticmethod
    def response_ok(data):
        response = jsonify({'status': 'success', 'data': data}, 200)
        return make_response(response)

    @staticmethod
    def response_error(message, error=None, error_code=None):
        response = json.dumps({'status': 'fail', 'message': message, 'error': error, 'error_code': error_code})
        return make_response(response, 400)


response = Response()
