from flask import Flask

from core import view as api


def create_app(config=None, testing=False, cli=False):
    """Application factory, used to create application
    """
    app = Flask('core')

    register_extension(app)
    configure_app(app, testing)
    register_blueprints(app)
    return app


def register_extension(app):
    # from core.extensions import redis_store
    # redis_store.init_app(app)
    return True


def configure_app(app, testing=False):
    """set configuration for application
    """
    # default configuration
    app.config.from_object('core.config')

    if testing is True:
        # override with testing config
        app.config.from_object('core.configtest')
    else:
        # override with env variable, fail silently if not set
        app.config.from_envvar("API_CONFIG", silent=True)


def register_blueprints(app):
    """register all blueprints for application
    """
    app.register_blueprint(api.blueprint)
