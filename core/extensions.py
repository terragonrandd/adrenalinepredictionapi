"""Extensions module. Each extension is initialized in the app factory located in app.py."""
from flask_wtf.csrf import CSRFProtect
from flask_restful import Api


csrf_protect = CSRFProtect()
api = Api()