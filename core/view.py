import pandas

from flask import Blueprint, request
from core.extensions import csrf_protect
from core.utils import response, check_auth_header
import datetime
from sklearn.externals import joblib
from pandas import DataFrame, get_dummies

blueprint = Blueprint('api', __name__, url_prefix='/api/v1')


@blueprint.route('/message', methods=['GET'])
@csrf_protect.exempt
def messaging_service():
    view_class = MessageHandlerService()
    return view_class.get()


class MessageHandlerService(object):
    def get(self):
        validate_auth_header = check_auth_header()
        if not validate_auth_header[0]:
            return validate_auth_header[1]
        action = request.args.get('action')
        if not action:
            return response.response_error("No action passed. You need to specify an action.")
        if action == "gettype":
            message = request.args.get('message')
            return response.response_ok(message)


@blueprint.route('/predict', methods=['POST'])
@csrf_protect.exempt
def prediction_service():
    prediction_class = PredictionService()
    return prediction_class.post()


class PredictionService(object):
    def post(self):
        campaign_types = {'business_and_finance': 'models/finance.pkl', 'event': 'models/event.pkl',
                          'tech': 'models/tech.pkl', 'education': 'models/education.pkl',
                          'family': 'models/family.pkl', 'sports': 'models/sports.pkl',
                          'religion': 'models/religion.pkl', 'healthy_living': 'models/healthy_living.pkl'
                          }
        data_columns = ['age', 'dataRevenue', 'gender', 'totalRevenue', 'vasRevenue', 'day', 'hour', 'model',
                        'customerClass', 'region', 'city', 'state', 'customerValue']

        encoded_cat = ['os_type', 'region', 'day', 'hour', 'model', 'state', 'city', 'customerValue', 'customerClass']

        data_column_holder = ['age', 'dataRevenue', 'gender', 'totalRevenue', 'vasRevenue', 'os_type_1.0',
                              'os_type_2.0', 'os_type_3.0', 'os_type_4.0', 'os_type_26.0', 'region_1.0', 'region_2.0',
                              'region_3.0', 'region_4.0', 'region_5.0', 'region_6.0', 'region_7.0', 'region_8.0',
                              'day_Fri', 'day_Mon', 'day_Sat', 'day_Thu', 'day_Tue', 'day_Wed', 'hour_9', 'hour_10',
                              'hour_11', 'hour_12', 'hour_13', 'hour_14', 'hour_15', 'hour_16', 'hour_17', 'model_67.0',
                              'model_70.0', 'model_104.0', 'model_301.0', 'model_354.0', 'state_14.0', 'state_32.0',
                              'state_68.0', 'state_178.0', 'state_195.0', 'city_2.0', 'city_7.0', 'city_12.0',
                              'city_32.0', 'city_37.0', 'customerValue_1', 'customerValue_2', 'customerValue_3',
                              'customerValue_4', 'customerValue_5', 'customerClass_30', 'customerClass_32',
                              'customerClass_39', 'customerClass_41', 'customerClass_44', 'customerClass_46']

        validate_auth_header = check_auth_header()
        if not validate_auth_header[0]:
            return validate_auth_header[1]
        campaign_type = request.args.get('campaign_type')
        if not campaign_type:
            return response.response_error("No campaign type passed")
        if campaign_type not in campaign_types.keys():
            return response.response_error("Invalid Campaign type passed. Campaign types "
                                           "should be one of {}".format(campaign_types))
        upload_type = request.args.get('upload_type')
        print(upload_type)
        if upload_type == "csv":

            file_ = request.files.get('file')
            if not file_:
                return response.response_error("No File passed")
            request_data = pandas.read_csv(file_)
            for col in data_columns:
                if col not in request_data.columns:
                    return response.response_error("Field '{0}' is missing".format(col))
        else:
            request_data = request.json
            if not request_data:
                return response.response_error("No request data passed")
            for i in data_columns:
                if i not in request_data.keys():
                    return response.response_error("Field '{0}' is missing".format(i))
        clf = joblib.load(campaign_types[campaign_type])
        request_as_df = DataFrame(request_data)
        request_as_df[encoded_cat] = request_as_df[encoded_cat].astype('category')
        request_as_dummy = get_dummies(request_as_df[encoded_cat])
        request_as_dummy = request_as_dummy.reindex(columns=data_column_holder, fill_value=0)
        prediction = clf.predict_proba(request_as_dummy).tolist()
        output_map = {'campaign_type': campaign_type,
                      'predicted_on': datetime.datetime.now()}
        for i in prediction:
            for j in range(len(prediction)):
                output_map["click_probability_percentage_" + str(j)] = "{0}".format(i[1] * 100)
                output_map["no_click_propabability_percentage_" + str(j)] = "{0}".format(i[0] * 100)
        return response.response_ok(output_map)
